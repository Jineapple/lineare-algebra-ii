\section{Euklidische und unitäre Vektorräume}
\begin{definition}
Sei \(V\) ein \(\mathbb{R}\)-Vektorraum. 
\begin{enumerate}
\item Eine symmetrische, positiv definierte Bilinearform
  \(<\cdot,\cdot>: V\times V \rightarrow \mathbb{R}\)
  heißt Skalarprodukt auf \(V\)
\item Ein \(\mathbb{R}\)-Vektorraum \(V\) zusammen mit einem
  Skalarprodukt 
  \(<\cdot,\cdot>: V\times V \rightarrow \mathbb{R}\)
  heißt euklidischer Vektorraum \((V, <\cdot, \cdot>)\)
\end{enumerate}
\end{definition}

\begin{example}
\begin{enumerate}
\item \(<\cdot,\cdot>: \mathbb{R}^n\times \mathbb{R}^n \rightarrow
  \mathbb{R}\) mit \(<v,w> = v^T w\) ein Skalarprodukt\\
  (Auch Standardskalarprodukt auf \(\mathbb{R}^n\) oder euklidisches
  Skalarprodukt auf \(\mathbb{R}^n\))
  \begin{itemize}
    \item Bilinearform: klar
    \item Symmetrisch: \(<v,w> = v^Tw = (v^Tw)^T = w^t v = <w, v>\)
    \item Positiv definiert: \(<v,v> = v^Tv = [v_1, \hdots, v_n] 
      \begin{bmatrix}
        v_1\\
        \vdots\\
        v_n
      \end{bmatrix} = v_1^2 +\hdots + v_n^2 > 0 \, \forall v \in
      \mathbb{R}^n \backslash{0}
      \)
  \end{itemize}
\item \(V = (C([a,b])), <\cdot, \cdot>: V\times V \rightarrow
  \mathbb{R}\) 
  mit \(<f,g> = \int\limits_a^b f(t) g(t) dt\)
  
\item \(A = \begin{bmatrix}1 & -2\\ -2 & 1\end{bmatrix} 
  \beta \mathbb{R}^2 \times \mathbb{R}^2 \rightarrow \mathbb{R}\) mit
  \(\beta(v,w) = v^T A w\) kein Skalarprodukt 
\[
v = \begin{bmatrix}1\\1\end{bmatrix}\, \beta(v, v) = v^T A v = 
\begin{bmatrix}
  1 & 1
\end{bmatrix}
\begin{bmatrix}
1 & -2\\
-2 & 1
\end{bmatrix}
\begin{bmatrix}
1\\
1
\end{bmatrix}
= -2 < 0\lightning_{\beta(v,v) > 0 \forall v \in V \backslash\{0\}}
\]
\item 
\end{enumerate}
\end{example}
\(f: V \times V \rightarrow \mathbb{R}\) Bilinearform + symetrisch und
positiv definit \(\Rightarrow <\cdot,\cdot>: V \times V \rightarrow
\mathbb{R} (V, <\cdot, \cdot>)\)
\begin{target} Verallgemeinerung für \(K = \mathbb{C}\)
Sei \(V = \mathbb{C}^n\)\\
Falls 
\begin{align*}
\beta(v, v) > 0 \forall v \in \mathbb{R}^n\backslash\{0\} \Rightarrow
& \beta(iv, iv) = i^2\beta(v, v) = -\beta(v, v) < 0\\
& \beta(\lambda v, \lambda v) = \lambda^2 \beta(v, v)\\
\lambda = a + ib \Rightarrow
&\lambda^2 = (a + ib)^2 = a^2 -b^2 + 2 iab\\
&\lambda\overline{\lambda} =|\lambda|^2 > 0 \forall \lambda \neq 0
\end{align*}
\end{target}
\begin{definition}
Seien \(V, W \mathbb{C}\)-Vektorräume.
\begin{enumerate}
\item Eine Abbildung \(f: V \rightarrow W\) heißt \emph{semilinear},
  falls 
\begin{align*}
f(v_1 + v_2) &= f(v_1) + f(v_2) &&\forall v_1, v_2 \in V\\
f(\lambda v) &= |\lambda| f(v) && \forall v\in V, \lambda \in \mathbb{C}
\end{align*}
\item Eine Abbildung heißt \emph{Sesquilinearform}, falls 
\begin{enumerate}
\item \(\beta(\cdot, w) : V \rightarrow C, v\mapsto \beta(v,w)\) für
  alle \(w \in W\) semilinear ist
\item \(\beta(v, \cdot) : V \rightarrow C, w\mapsto \beta(v,w)\) für
  alle \(v \in W\) semilinear ist
\end{enumerate}
\item Eine Sesquilinearform 
  \(\beta: V\times V \rightarrow \mathbb{C}\) 
  heißt Hermitesche Form, falls \(\beta(v,w) = \overline{\beta(w,v)}
  \forall v, w \in V\)
\item Eine Sesquilinearform heißt positiv definit, falls 
  \(\beta(v,v)> 0 \forall v \in V\)
\item Eine hermitesche, positiv definite Form 
  \(<\cdot, \cdot >: V \times V \rightarrow \mathbb{C}\)
  heißt Skalarprodukt auf \(V\)
\item Ein \(\mathbb{C}\)-Vektorraum zusammen mit einem Skalarprodukt 
  \(<\cdot, \cdot >: V\times V \rightarrow \mathbb{C}\) heißt
  \emph{unitärer Vektorraum} \((V, <\cdot, \cdot >)\)
\end{enumerate}
\end{definition}
\begin{example}
\begin{enumerate}
\item Sei \(\beta: \mathbb{C}^n \times \mathbb{C}^n\) eine
  Bilinearform auf \(\mathbb{C}^n \times \mathbb{C}^n\), dann ist
  \(\tilde{\beta}: \mathbb{C}^n \times \mathbb{C}^n\) mit
  \(\tilde{\beta}(v, w) = \beta(\overline{v}, w)\) eine
  Sesquilinearform
\item \(<,>: \mathbb{C}^n \times \mathbb{C}^n \rightarrow \mathbb{C}\)
  mit \(<v,w>:= \overline{v}^Tw = \sum\limits_{i=1}^n \overline{v_i}
  w_i\) ist ein Skalarprodukt auf \(\mathbb{C}^n\)
  \begin{enumerate}
    \item Sesquilinearität: klar
    \item Hermitesche Form: 
      \[<v,w> = \sum_{i=1}^n \overline{v}_i w_i = 
        \overline{\sum_{i=1}^n\overline{w}_i v_i} = <\overline{w,v}>
        \, \forall v, w \in \mathbb{C}^n\]
    \item positiv definit: 
      \[<v, v> = \sum_{i=1}^n \overline{v}_i v_i = \sum_{i=1}^n
        |v_i|^2 > 0 \forall v \in \mathbb{C}^n\backslash\{0\}\]
  \end{enumerate}
\(\Rightarrow (\mathbb{C}^n, <\cdot, \cdot>)\) - unitärer Vektorraum
\end{enumerate}
\end{example}
\begin{definition}
\begin{enumerate}
\item Sei \(V\) ein \(\mathbb{C}\)-Vektorraum mit Basis 
  \(B = (v_1, \hdots, v_n)\) und sei \(\beta: V\times V \rightarrow
  \mathbb{C}\) eine Sesquilinearform. Die Matrix 
  \(G_B := G_B(\beta) = [\beta(v_i, v_j)]_{i,j = 1}^{n,n} \in
  \mathbb{C}^{n\times n}\) heißt Gram-Matrix von \(\beta\) bzgl \(B\)
\item Sei \(A \in \mathbb{C}^{n\times m}\), dann heißt \(A^* =
  \overline{A}^T \in \mathbb{C}^{m \times n}\) die komplex
  konjungierte transponierte von \(A\) (auch \(A^H\)).
\item Eine Matrix \(A \in \mathbb{C}^{n \times n}\) heißt hermitesch,
  falls \(A^* = A\)
\item Eine Matrix \(A \in \mathbb{C}^{n\times n}\) heißt positiv
  defnit, falls \(v^*Av > 0 \forall v \in \mathbb{C}^n\backslash\{0\}\)
\end{enumerate}
\begin{example}
\begin{enumerate}
\item 
\[
  A = 
  \begin{bmatrix}
    2 + i & 3 & -i\\
    4 - 2i& i & 1+i\\
  \end{bmatrix}
  ,
  A^* = 
  \begin{bmatrix}
    2 - i& 4 + 2i\\
    3 & i\\
    i & 1-i\\
  \end{bmatrix}
\]
\item 
\(
A = 
\begin{bmatrix}
  2 & 3+2i\\
  2-3i & 1\\
\end{bmatrix}, 
A^* = 
\begin{bmatrix}
  2 & 3+2i\\
  2-3i & 1\\
\end{bmatrix} 
= A\) hermitesch.
Für \(v = \begin{bmatrix}1\\i\end{bmatrix}\): 
\[v^*Av= [1, -i] 
  \begin{bmatrix}
    2 & 3+2i\\
    2-3i & 1\\
  \end{bmatrix}
  \begin{bmatrix}
    1\\
    i\\
  \end{bmatrix}
  = -1 < 0 \text{ nicht positiv definit}
\]
\end{enumerate}
\end{example}
\end{definition}

\begin{remark}
\begin{enumerate}
\item \(A = [a_{ij}]_{i,j = 1}^n\) hermitesch \(\Rightarrow a_{ij} =
  \overline{a}_{ji}\)
\item \(A + B\) hermitesch, \(\lambda A\) hermitesch 
  \(\{A \in \mathbb{C}^{n\times n}| A \text{hermitesch} \}\) ist ein
  \(\mathbb{R}\)-Vektorraum, aber kein \(\mathbb{C}\)-Vektorraum
\[(iA)^* = (\overline{iA})^T = -iA^* = -iA \neq iA \text{ für } A \neq
  0\]
\item Sei \(A \in \mathbb{C}^{n\times n}\) hermitesch, so ist \(\beta:
  \mathbb{C}^n \times \mathbb{C}^n \rightarrow \mathbb{C}\) mit
  \(\beta(v, w) = v^*Aw\) hermitesche Form
\item Sei \(V\) ein \(\mathbb{C}\)-Vektorraum mit Basis \(B\) und
  \(\beta: V\times V \rightarrow \mathbb{C}\) eine Sesquilinearform 
  \begin{enumerate}
    \item \(\beta\) ist hermitesche Form 
      \(\Leftrightarrow G_B(\beta)\) ist hermitesch
    \item \(\beta\) ist positiv definit
      \(\Leftrightarrow G_B(\beta)\) ist positiv definit
  \end{enumerate}
  \item Sind \(B_1, B_2\) Basen von \(V\) und \(\beta: V\times V
    \rightarrow \mathbb{C}\) eine Sesquilinearform, so gilt
    \(G_{B_1}(\beta) = S^* G_{B_2}(\beta) S\) mit \(S = [id_V]_{B_2}^{B_1}\)
\end{enumerate}
\end{remark}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
