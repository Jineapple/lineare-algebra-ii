\section{Dualräume und Linearformen}
\(V\) ist ein \(K\)-Vektorraum, \(K\) ein Körper.
\begin{definition}
\begin{enumerate}
\item \(V^* := \mathscr{L}(V,K) = 
  \{\varphi : V\rightarrow K| \varphi \text{ linear}\}\) heißt
  Dualraum von \(V\). 
\item \(\varphi \in V^*\) heißt \emph{Linearform} auf \(V\).
\end{enumerate}
\end{definition}
\begin{example}
\begin{enumerate}
\item \(V = K^n, A = [a_1, \hdots, a_n] \in K^{1\times n}\)
\[\varphi(v) = Av = [a_1, \hdots, a_n] 
  \begin{bmatrix}v_1\\\vdots\\v_n\\\end{bmatrix} 
  = a_1v_1 + \hdots + a_n v_n \in K\]
\item \(V = C([a,b]) = 
  \{f:[a,b] \rightarrow \mathbb{R}| f \text{ stetig}\}, 
  c \in [a,b]\) 
  \begin{align*}
    \varphi_1 : V &\rightarrow \mathbb{R} 
    &\varphi_2: V &\rightarrow \mathbb{R}\\
    f &\mapsto \int_a^bf(t) dt & f &\mapsto f(c)
  \end{align*}
\end{enumerate}
\end{example}

\begin{theorem}
Sei \(V\) ein endlich dimensionaler Vektorraum mit Basis 
\(B = (v_1,\hdots, v_n)\). Dann gibt es eindeutig bestimmte
Linearformen 
\(v_1^*, \hdots, v_n^* \in V^*\) mit 
\[v_i^*(v_j) = \delta_{ij} = 
  \begin{cases}
    1 & i = j\\
    0 & i \neq j\\
  \end{cases}
\]
Ferner gilt \(B^* = (v_1^*, \hdots, v_n^*)\) einen Basis von \(V^*\)
und somit \(dim(V^*) = dim(V)\)

\begin{proof}
Sei \(i \in \{1, \hdots, n\}, B = (v_1, \hdots, v_n)\) eine Basis von
\(V, \delta_{i1}, \hdots, \delta_{ik} \in K\).\\
\(\xRightarrow{S4.1}\) es gibt genau eine lineare Abbildung
 \(v_i^* : V \rightarrow K\) mit 
 \(v_i^*(v_j) = \delta_{ij}, j=1, \hdots, n\). 
 \[dim(V*) = dim(\mathscr{L}(V, K)) = dim(K^{1 \times n}) = n =
   dim(V)\]
da \(\mathscr{L}(V,K) \cong K^{1\times n}\) (S 4.5).\\
\(z.z: v_1^*, \hdots, v_n^*\) linear unabhängig 
\(\left(\Rightarrow B^{\ast}  = (v_1^*, \hdots, v_n^*) \text{ Basis von
  }V^*\right)\)\\
Seien \(\lambda_1, \hdots, \lambda_n \in K\), s.d
\(\sum\limits_{i=1}^n \lambda_iv_i^* = O_{V^*}\)
\begin{align*}
\Rightarrow 0 
&= 0_{V^*}(v_j)\\
&= \sum_{i=1}^n(\lambda_iv_i^*)(v_j)\\
&= \sum_{i=1}^n\lambda_i v_i* (v_j)\\
&= \lambda_j, j=1, \hdots, n
\end{align*}
\(\Rightarrow v_1^*, \hdots, v_n^*\) linear unabhängig.
\end{proof}
\end{theorem}
\begin{definition}
\(B^*=(v_1^*, \hdots, v_n^*)\) heißt die zu \(B=(v_1, \hdots, v_n)\)
duale Basis von \(V^*\).
\end{definition}
\begin{example}
\(V = K^n, B = (e_1, \hdots, e_n), e_i^* \in (K^n)^* \cong K^{1 \times
    n}\)
\begin{align*}
e_i^*(v) &= e_i^Tv && \forall v \in K^n \\
v_i^*(e_j) &= e_i^Te_j = d_{ij} && i = 1, \hdots,n
\end{align*}
\((e_1^*, \hdots, e_n^*)\) duale Basis von \((K^n)^*\).
\end{example}

\begin{definition}
Sei \(V\) ein \(K\)-VR. Dann heißt der Raum \(V^{**} = (V^*)^*\) der
Bidualraum von \(V\).
\end{definition}
\begin{remark}
\(V^{**} = \{f:V^* \rightarrow K|f \text{ linear}\}\)
\end{remark}
\begin{example}
Sei \(v \in V, f_v
\begin{array}{r c l} 
   V^* & \rightarrow & K\\
  \varphi & \mapsto & \varphi(v)\\
\end{array}\) linear.
\begin{align*}
f_v(\varphi_1 + \varphi_2) &= (\varphi_1 + \varphi_2)(v) =
                             \varphi_1(v) + \varphi_2(v) = 
                             f_v(\varphi_1) + f_v(\varphi_2)\\
f_v(\lambda \varphi &= (\lambda \varphi)(v) = \lambda f_v(\varphi)
\end{align*}
gilt 
\(\forall \varphi_1, \varphi_2, \varphi \in V^*, \forall \lambda \in K\)
\end{example}
\begin{theorem}
Sei \(v \in V\backslash\{0\}\), dann gibt es ein 
\(\varphi \in V^* : \varphi(v) \neq 0\)

\begin{proof}
Sei \(v_1, \hdots, v_n\) eine Basis von \(V, (v_1^*, \hdots, v_n^*)\)
eine duale Basis.\\
\(\Rightarrow  \exists \lambda_1, \hdots, \lambda_n \in K: 
v = \sum\limits_{j=1}^n \lambda_jv_j\). Da \(v \neq 0\) gibt es ein
\(i \in \{1, \hdots, n\}\) mit \(\lambda_i \neq 0\).
\begin{align*}
\Rightarrow 
&v_i^*(v) = v_i^*\left(\sum_{j=1}^n \lambda_jv_j\right) 
            = \sum_{j=1}^n \lambda_j v_i^*(v_j) = \lambda_j \neq 0\\
\Rightarrow & \varphi_i = v_i^*
\end{align*}
\end{proof}
\end{theorem}
\begin{theorem}
Sei \(V\) ein \(K\)-VR. Dann ist die Abbildung 
\begin{align*}
f: V &\rightarrow V^{**} &\text{mit } f_v := V^* &\rightarrow K\\
v &\mapsto f_v & \varphi &\mapsto \varphi(v)
\end{align*}
ein Monomorphismus, dh. linear und injektiv. 
Ist \(dim(V) = n < \infty\), so ist \(f\) sogar ein Isomorphismus, dh
\(V \cong V^{**}\). 

\begin{proof}
\begin{itemize}
\item \(z.z: f\) linear\\
Seien 
\(v, w \in V \Rightarrow f(v+w) = f_{v+w} = f_v + f_w = f(v) + f(w)\)
\[f_{v+w}(\varphi) = \varphi(v + w) = \varphi(v) + \varphi(w) =
  f_v(\varphi) + f_w(\varphi) = (f_v +f_w) (\varphi)\, \forall \varphi
  \in V^*\]
Sei \(v \in V, \lambda \in K \Rightarrow f(\lambda v) = f_{\lambda v}
= \lambda f_v = \lambda f(v)\)
\[f_{\lambda v}(\varphi) = \varphi(\lambda v) = \lambda \varphi(v) =
  \lambda f_v (\varphi) \, \forall \varphi \in V^*\]

\item \(z.z: f\) injektiv\\
Sei \(f(v_1) = f(v_2)\) für \(v_1, v_2 \in V\). Sei \(v = v_1 - v_2\)
\begin{align*}
\Rightarrow
& f(v) = f(v_1) - f(v_2) = 0_{V^{**}}\\
\Rightarrow 
&\forall \varphi \in V^*: 0_{V^{**}} = 0_K = f_v(\varphi) =
  \varphi(v)\\
\Rightarrow
&v = 0\text{ sonst }\exists \varphi \in V^*: \varphi(v) \neq 0 \text{
  nach S 7.2}\\
\Rightarrow
& v_1 = v_2\\
\Rightarrow
& f \text { injektiv}
\end{align*}

\item \(z.z:\) Ist \(dim(V) <\infty\), so ist \(f\) surjektiv\\
Sei \((v_1, \hdots, v_n)\) eine Basis von \(V\)
\begin{align*}
\Rightarrow 
& (v_1^*, \hdots, v_n^*) \text{ duale Basis von }V^*\\
\Rightarrow 
&(v_1^{**}, \hdots, v_n^{**}) \text{ zu } B^* \text{ duale
  Basis von } V^{**}\\
\Rightarrow 
& dim(V) = dim(V^{**}) + f \text{ injektiv}\\
\Rightarrow 
& f \text{ surjektiv}\\
\Rightarrow
& f \text{ Isomorphismus}
\end{align*}
\end{itemize}
\end{proof}
\end{theorem}

\begin{definition}
Seien \(V, W K\)-VR, \(f: V\rightarrow W\) linear.\\
Dann heißt 
\(f^*: 
\begin{array}{r c l}
  W^* &\rightarrow &V^*\\
  \psi & \mapsto & \psi \circ f
\end{array}\) zu \(f\) duale Abbildung.
\end{definition}
\begin{remark}
\begin{enumerate}
\item ~\\
	\includegraphics[width=.35\textwidth]{img/07-01-dualeabbildung.pdf}
\item \(f^*: W^* \rightarrow V^*, f^*(\psi) = \psi \circ f\) linear
  (Übung)
\end{enumerate}
\end{remark}
\begin{theorem}
Seien \(V, W K\)-VR. \(B_V = (v_1, \hdots, v_n)\) Basis von \(V\), 
\(B_W = (w_1, \hdots, w_n)\) Basis von \(W\), sowie 
\(f: V \rightarrow W\) linear.\\
Dann gilt
\[[f^*]_{B_V^*}^{B_W^*} = \left([f]_{B_W}^{B_V}\right)^T\]
\begin{proof}
\(A = [a_{ij}] = [f]_{B_W}^{B_V} \in K^{m \times n}, 
 C = [c_{ij}] = [f^*]_{B_V^*}^{B_W^*} \in K^{n \times m}\) 
\begin{align*}
f(v_k) 
&= \sum_{i=1}^m a_{ik}w_i && k=1, \hdots,n\\
f^*(w_j^*) 
&= \sum_{i=1}^n c_{ij}v_i^* && j = 1, \hdots, m\\
c_{kj}
&= \sum_{i=1}^n c_{ij} \underbrace{v_i^*(v_k)}_{\delta_{ij}}\\
&= \left(\sum_{i=1}^n c_{ij}v_i^*\right)v_k)\\
&= f^*(w_j*)(v_k)\\
&= (w_j^*\circ f)(v_k)\\
&= w_j^*(f(v_k))\\
&= w_j^*\left(\sum_{i=1}^m a_{ik}w_i\right)\\
&= \sum_{i=1}^m a_{ik}w_j^*(w_i)\\
&= a_{jk}\\
\Rightarrow c 
&= A^T
\end{align*}
\end{proof}
\end{theorem}
\begin{remark}
Seien \(V, W K\)-VR, \(f \in \mathscr{L}(U,V), g \in \mathscr{L}(V,
W)\).
\begin{enumerate}
\item \((g\circ f)^* = f^* \circ g^*\)~\\
\((AC)^T = C^T A^T\)
\item Ist \(f\) bijektiv, so ist \(f^*\) bijektiv und \((f^*)^{-1} =
  (f^{-1})^*\).\\
\((A^T)^{-1} = (A^{-1})^T\)
\end{enumerate}
\end{remark}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
