\section{Orthogonalität und Orthonormalbasis}
\(V\) K-Vektorraum\\
\(K = \mathbb{R}\) oder \(\mathbb{C}\) mit Skalarprodukt
\(<\cdot,\cdot>\) und zugehörige Norm \(||\cdot||\)
\[V = \mathbb{R}, -1 \leq \frac{<v,w>}{||v||||w||} \leq 1\]

\begin{definition}
Die Zahl \(\varphi \in [0, \pi]\) mit \(\cos\pi =
\frac{<v,w>}{||v||||w||}\) heißt Winkel zwischen 
\(v \in V\) und \(w \in W\)
\begin{align*}
<v,w> = 0 
&\Leftrightarrow \cos\varphi = 0\\
&\Leftrightarrow \varphi = \frac{\pi}{2}\\
&\Leftrightarrow v,w - \text{ orthogonal}
\end{align*}
\begin{center}
	\includegraphics[width=.4\textwidth]{img/07-05-orthogonalevektoren.pdf}
\end{center}
\end{definition}
\begin{definition}
Sei \(V\) ein euklidischer oder unitärer Vektorraum
\begin{enumerate}
\item \(v, w \in V\) heißen \emph{orthogonal}, falls \(<v,w> = 0
 \) \hspace{1em} \((v\perp w)\)
\item \(v_1, \hdots, v_n \in V\) heißen orthogonal, falls 
  \(<v_i, v_j> = 0\),\(i = 1, \hdots, n, i \neq j\)
\item \(v_1, \hdots, v_n \in V\) heißen normiert, falls 
  \(||v_i|| = 1, i = 1, \hdots, n\) 
\item \(v_1, \hdots, v_n \in V\) heißen orthonormal, falls sie
  orthogonal und normiert sind
\item \((v_1,\hdots, v_n)\) heißt Orthonormalbasis von \(V\), falls
  \(v_1, \hdots, v_n\) orthonormal sind und eine Basis von \(V\)
  bilden
\end{enumerate}
\end{definition}
\begin{example}
\begin{enumerate} 
\item \(V = \mathbb{R}^2, e_1 = \begin{bmatrix}1\\0\end{bmatrix}, 
  e_2 = \begin{bmatrix}0\\1\end{bmatrix}, 
  <e_1, e_2> = 1\cdot 0 + 0 \cdot 1 = 0\) ~\\
  \( ||e_1||=1=||e_2||, (e_1, e_2) - \) Basis
\item \(v_1 = \begin{bmatrix}1\\1\end{bmatrix}, 
  v_2 = \begin{bmatrix}1\\-1\end{bmatrix}, <v_1, v_2> = 0 \Rightarrow
  v_1, v_2 \) orthogonal\\
  \(||v_1||_2 = \sqrt{2} =||v_2||_2\) nicht normiert
\item \(\cos(t), \sin(t), t \in [-\pi, \pi], 
  \int\limits_{-\pi}^\pi \cos(t) \sin(t) dt = 0\)
\end{enumerate}
\end{example}

\begin{remark}
Seien \(v_1, \hdots, v_n \in V \backslash \{0\}\) orthogonal. Dann
gilt: 
\begin{enumerate}
\item \(\lambda_1v_1, \hdots, \lambda_n v_n\) mit 
  \(\lambda_i =  \frac{1}{||v_i||}, i = 1, \hdots, n\) sind
  orthonormal, da
\begin{align*}
  <\lambda_i, \lambda_j v_j> &
=\overline{\lambda_i}\lambda_j\underbrace{<v_i,v_j>}_0 = 0
&&i \neq j\\
||\lambda_i v_i|| =|\lambda_i|||v_i|| 
&= \frac{||v_i||}{||v_i||} = 1 
&&i = 1,\hdots, n
\end{align*}
\item \(v_1,\hdots, v_n\) sind linear unabhängig, da 
  \(\sum\limits_{j=1}^n\alpha_j v_j = 0\)
\begin{align*}
\Rightarrow 
& 0 = <v_i, \sum_{j=1}^n \alpha_j v_j> = \sum_{j=1}^n\alpha_j<v_i,v_j>
  = \alpha_i<v_i,v_i>\\
\Rightarrow 
& \alpha_i = 0 && i = 1, \hdots, n\\
\Rightarrow
& v_1, \hdots, v_n  \text{ linear unabhängig}
\end{align*}
\end{enumerate}

\end{remark}

\begin{theorem}
Sei \(B = (v_1, \hdots, v_n)\) eine ONB von \(V\). Dann gilt 
\[v= \sum_{i=1}^n <v_i,v> v_i\, \forall v \in V\]
d.h: 
\(\varphi_B(v) = 
\begin{bmatrix}
<v_1, v>\\
\vdots\\
<v_n, v>\\
\end{bmatrix}
\)
\begin{proof}
Sei \(v \in V\).
\begin{align*}
\Rightarrow v 
&= \sum_{j=1}^n \lambda_j v_j\\
\Rightarrow <v_i, v> 
&= <v_i, \sum \lambda_j v_j>\\
&= \sum_{j=1}^n \lambda_j <v_i, v_j>\\
&= \lambda_i && i = 1, \hdots, n
\end{align*}
\end{proof}
\end{theorem}

\begin{example}
\(V = \mathbb{R}^2, (u_1, u_2)\) Basis von \(\mathbb{R}^2\)
\begin{enumerate}
\item Normiere \(u_1 : v_1 = \frac{u_1}{||u_1||} \Rightarrow Span\{v_1\}
  = Span\{u_1\}\)
\item Finde \(w\) so, dass \(w = u_2 - \lambda v_1 \perp v_1\)
\begin{align*}
\Rightarrow & 
0 = <v_1, w> = <v_1, u_2 - \lambda v_1> = <v_1, u_2> 
- \lambda <v_1,v_1>\\
\Rightarrow &
\lambda = \frac{<v_1, u_2>}{<v_1,v_1>}\\
w = u - \frac{<v_1, u_2>}{<v_1,v_1>} \cdot v_1, v_2 := \frac{w}{||w||} \Rightarrow
& (v_1, v_2)  - \text{ ONB}
\end{align*}
\end{enumerate}
\end{example}

\begin{theorem}[Orthonormalisierungsverfahren von Gram-Schmidt]
Sei \(V\) ein euklidischer oder unitärer Vektorraum mit Skalarprodukt
\(<\cdot, \cdot>\) und sei \((u_1, \hdots, u_n)\) eine Basis von
\(V\). 
\begin{enumerate}
\item Setze 
\[w_1 = u_1, w_i = u_i - 
  \sum_{j=1}^{i-1}  \frac{<w_j, u_i>}{<w_j, w_j>} w_j,\, i = 2, \hdots,
  n\]
Dann ist \((w_1, \hdots, w_n)\) eine orthogonale BAsis von \(V\) und
\[Span\{w_1, \hdots, w_k\} = Span\{u_1, \hdots, u_k\}, k = 1, \hdots,
n\]
\item Setze 
  \[v_1 = \frac{u_1}{||u_1||}, \tilde{v_i} = u_i - 
    \sum_{j=1}^{i-1} <v_j, u_i>v_j, v_i =
    \frac{\tilde{v}_i}{||\tilde{v}_i||}, 
    i = 2, \hdots, n\]
Dann ist \((v_1, \hdots, v_n)\) eine ONB von \(V\) und 
\[Span\{v_1, \hdots, v_k\} = Span\{u_1, \hdots, u_k\}\, k = 1, \hdots,
  n\]
\end{enumerate}
\begin{proof}
\begin{enumerate}
\item Induktion nach \(n\)
 \begin{itemize}
   \item \(\mathbf{n=2}:\) 
     \(w_1 = u_1 \neq 0, w_2 = u_2 - \frac{<w_1, u_2>}{<w_1, w_1>}
     \underbrace{w_1}_{u_1} \neq 0\), da \(u_1, u_2\) linear
     unabhängig
     \begin{align*}
       <w_1, w_2> 
       &= <w_1, u_2 - \frac{<w_1, u_2>}{<w_1, w_1>} w_1> \\
       &= <u_1, u_2> - \frac{<u_1, u_2>}{<w_1, w_1>}<w_1, w_1> = 0
     \end{align*}
     \(Span\{w_1\} = Span\{u_1\}, Span\{w_1, w_2\} = Span\{u_1,
     u_2\}\), da \(w_1 = u_1\) und 
     \(w_2 = u_2 - \alpha u_1 = u_2 - \alpha w_1\)
   \item \(\mathbf{n-1 \rightarrow n}:\) Seien \(w_1, \hdots,
     w_{n-1}\) orthogonal, \(w_j \neq 0, j = 1, \hdots, n-1\).
     \(Span\{w_1, \hdots, w_k\} = Span\{u_1, \hdots, u_k\}, k = 1,
     \hdots, n-1\)
     \begin{align*}
       \Rightarrow <w_i, w_n> 
       &= <w_i, u_n - \sum_{j=1}^{n-1}\frac{<w_j, u_n>}{<w_j, w_j>}
         w_j>\\
       &= <w_i, u_n> - \sum_{j=1}^{n-1}
         \frac{<w_j, u_n>}{<w_j,w_j>}<w_j,w_j>\\
       &= <w_i, u_n> - <w_i, u_n> = 0 && i = 1, \hdots, n-1
     \end{align*}
     \(w_n \neq 0\), da sonst 
     \begin{align*}
       0 = w_n
       &= a_n \sum_{j=0}^{n-1} \frac{<w_j, u_n>}{<w_j, w_j>} w_j\\
       \Rightarrow u_n \in Span\{w_1, \hdots, w_{n-1}\}
       &= Span \{w_1, \hdots, w_{n-1}\}\lightning\\
       w_n 
       &= u_n - 
       \underbrace{\sum_{j=1}^{n-1} \frac{<w_j,u_n>}{<w_j, w_j>} w_j}
         _{Span\{w_1, \hdots, w_{n-1}\} = Span\{u_1, \hdots,
         u_{n-1}\}}\\
       \Rightarrow w_n \in Span \{u_1, \hdots, u_n\}
     \end{align*}
 \end{itemize}
\item analog
\end{enumerate}
\end{proof}
\end{theorem}
\begin{example}
\(u_1 = 
\begin{bmatrix}
1\\ 2\\ -2\\
\end{bmatrix}, 
u_2 = 
\begin{bmatrix}
3\\ 3\\ 0\\
\end{bmatrix}, 
u_3 = 
\begin{bmatrix}
1\\0\\-4\\
\end{bmatrix}
\in \mathbb{R}^3
\)
Gram-Schmidt-Orthogonalisierung:\\
\begin{enumerate}
\item \(||u_1||_2 = \sqrt{1 + 4 + 4} = 3, v_1 = \frac{u_1}{||u_1||_2}
  = \frac{1}{3}
  \begin{bmatrix}
    1\\ 2\\ -2\\
  \end{bmatrix}
\)
\item 
\begin{align*}
<v_1, v_2> 
&= \frac{1}{3} <
\begin{bmatrix}
1\\2\\-2\\
\end{bmatrix}, 
\begin{bmatrix}
3\\3\\0\\
\end{bmatrix}
> = \frac{1}{3} 9 = 3\\
\Rightarrow \tilde{v}_2 
&= u_2 - <u_1, u_2> v_1 = 
\begin{bmatrix}
3 \\ 3\\0\\
\end{bmatrix}
- \frac{3}{3}
\begin{bmatrix}
1\\2\\-2\\
\end{bmatrix}
=
\begin{bmatrix}
2\\1\\2\\
\end{bmatrix}\\
||v_2||_2 
&= \sqrt{4+1-4} = 3\\
\Rightarrow v_2 
&= \frac{1}{3}
\begin{bmatrix}
2\\1\\2\\
\end{bmatrix}
\end{align*}
\item 
\begin{align*}
<u_1, u_3> 
&= \frac{1}{3} <
  \begin{bmatrix}
    1 \\ 2 \\ -2\\
  \end{bmatrix}, 
  \begin{bmatrix}
    1\\0\\-4\\
  \end{bmatrix}
  > = 3\\
<u_2, u_3>
&= \frac{1}{3} <
  \begin{bmatrix}
    2 \\ 1 \\ 2\\
  \end{bmatrix}, 
  \begin{bmatrix}
    1\\0\\-4\\
  \end{bmatrix}
  > = -2\\
\Rightarrow \tilde{v}_3
&= u_3 - <v_1, u_3>v_1 - 
  <v_2,u_3>v_2\\
&= 
\begin{bmatrix}
1\\0\\-4\\
\end{bmatrix}
- \frac{3}{3}
\begin{bmatrix}
1\\2\\-2
\end{bmatrix}
+ \frac{2}{3}
\begin{bmatrix}
2\\1\\2
\end{bmatrix}\\
||\tilde{v}_3|| &= 3\\
\Rightarrow v_3 &= \frac{1}{3}
\begin{bmatrix}
2\\-2\\-1
\end{bmatrix}
\end{align*}
\end{enumerate}
\end{example}
\begin{theorem}
Sei \(B = (v_1, \hdots, v_n)\) eine ONB von \(V\). Dann gilt 
\[<v,w> = \sum_{j=1}^n <v, v_j><v_j, w>\, \forall v,w\in V\]
\begin{proof}
\begin{align*}
<v,w> 
&= <\sum_{i=1}^n <v_i, v> v_i, \sum_{j=1}^n <v_j, w> v_j>\\
&= \sum_{i=1}^n \overline{<v_i,v>}<v_i, \sum_{j=1}^n<v_j, w> v_j>\\
&= \sum_{i=1}^n<v,v_i>\sum_{j=1}^n<v_j, w> <v_i,v_j>\\
&= \sum <v,v_i><v_i, w>
\end{align*}
\end{proof}
\end{theorem}
\begin{theorem}
Sei \((v_1, \hdots, v_n)\) eine ONB von \(V\). Dann gilt 
\[||v||^2 = <v,v> = \sum_{i=1}^n|<v_i, v>|^2\, \forall v \in V\]
\begin{proof}
\begin{align*}
||v||^2 
&= <v,v>\\
&= \sum_{i=1}^n<v, v_i><v_i, v>\\
&= \sum_{i=1}^n \overline{<v_i, v>}<v_i, v>\\
&= \sum_{i=1}^n |<v_i, v>|^2
\end{align*}
\end{proof}
\end{theorem}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
